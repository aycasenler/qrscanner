package com.example.barcodeorqrcodescanner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions


class MainActivity : AppCompatActivity(), View.OnClickListener, EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {

    private var btnScan: Button? = null
    companion object {
        private const val RC_CAMERA_PERM = 1903
        var tvResult: TextView? = null
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }
    @AfterPermissionGranted(RC_CAMERA_PERM)
    private fun cameraTask(){
        if(EasyPermissions.hasPermissions(this, android.Manifest.permission.CAMERA)){
           // Toast.makeText(this,"İzin zaten verildi.", Toast.LENGTH_SHORT).show()
        }else{
            EasyPermissions.requestPermissions(this,"Devam etmek için izin vermelisiniz",
                RC_CAMERA_PERM,android.Manifest.permission.CAMERA)
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        Toast.makeText(this,"İzin verilmedi.",Toast.LENGTH_SHORT).show()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        Toast.makeText(this,"İzin verildi.",Toast.LENGTH_SHORT).show()
    }

    override fun onRationaleDenied(requestCode: Int) {
        Toast.makeText(this,"İstek iptal edilidi.",Toast.LENGTH_SHORT).show()
    }

    override fun onRationaleAccepted(requestCode: Int) {
       Toast.makeText(this,"İstek onaylandı.",Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        cameraTask()
    }

    fun init() {
        tvResult = findViewById(R.id.tvResult)
        btnScan = findViewById(R.id.btnScan)
        //webView = findViewById(R.id.webView)

        btnScan?.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnScan?.id -> {
                val intent = Intent(this, ScanActivity::class.java)
                startActivity(intent)
                Toast.makeText(this, "Scanning...", Toast.LENGTH_SHORT).show()
                btnScan?.isVisible = false
            }
        }

    }


}