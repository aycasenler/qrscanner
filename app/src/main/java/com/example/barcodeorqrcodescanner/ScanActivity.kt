package com.example.barcodeorqrcodescanner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.widget.Toast
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.widget.TextView

class ScanActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    private var mScannerView: ZXingScannerView? = null
   // private var webView: WebView? = null
    private var tvScanResult: TextView? = null

    override fun handleResult(result: Result?) {
        Toast.makeText(this, "This is your Text" + result?.getText(), Toast.LENGTH_SHORT);
        Log.e("tag", result?.getText()); // Prints scan results
        Log.e("tag", result?.getBarcodeFormat().toString())

        // MainActivity.tvResult?.text = result?.text
        scannerActivity()
        tvScanResult = findViewById(R.id.tvScanResult)
        tvScanResult?.text = result?.text

       // initWebView(result?.text.toString())
        //onBackPressed()

    }

    fun scannerActivity() {
        val actionBar = supportActionBar
        actionBar?.title = "Scanner"
        actionBar?.setDisplayHomeAsUpEnabled(true)
        //actionBar?.setBackgroundDrawable( ColorDrawable(Color.parseColor("#3F51B5")));
        setContentView(R.layout.activity_scan)

    }
    override fun onSupportNavigateUp(): Boolean {
        val intent = Intent(this, MainActivity::class.java);
        startActivity(intent)
        return true
    }

//    fun initWebView(url: String) {
//        webView = findViewById(R.id.webView)
//        webView?.webViewClient = WebViewClient()
//        // wv.settings.javaScriptEnabled = true
//        webView?.settings?.builtInZoomControls = true
//        webView?.settings?.displayZoomControls = false
//        webView?.loadUrl(url)
//
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScannerView = ZXingScannerView(this)
        setContentView(mScannerView)

    }

    override fun onResume() {
        super.onResume()
        mScannerView?.setResultHandler(this)
        mScannerView?.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera()
    }
}
